Ansible module to install a minimal container using systemd

# Adding a container

The role is able to install a container using yum/dnf on a systemd 
running system. To do that, just use the role like this:

```
- hosts: bastion
  roles:
  - role: systemd_container
    distro: Fedora
    name: container_web
    version: 23
```

This will install the container in /var/lib/machines and register it
with machinectl.

# Supported distributions in the container

For now, only Fedora and Centos 7 are supported. 
